fun main(){
    println("Hello!")

    //Collections- group of related data
    //Immutable (is read only) vs. Mutable (read and write)
    //Immutable -> Airlines = ["Cebu", "Pal"]
    //Mutable -> Users = []
    //List, Set, Map

    //List -> Stores data in a specified order and accessed by indices
    //Immutable list
//    val airlines: List<String> = listOf("Cebu Pacific", "PAL", "Airasia")
//    println(airlines)
//    println(airlines.get(1))
//    println(airlines[1])
//    println(airlines.size)
//
//    //Mutable list
//    val terminals: MutableList<Int> = mutableListOf(1,2,3,4)
//    println(terminals)
//    println(terminals.add(4,5))
//    println(terminals)
//    println(terminals.add(6))
//    println(terminals)
//    println(terminals.contains(5))
//    println(terminals.removeAt(5))
//    println(terminals)
//    println(terminals.remove(5))
//    println(terminals)
//
    //Activity 1
//    //Mutable List
//    val colors = mutableListOf("red", "orange", "blue", "violet")
//    println(colors)
//    println(colors.get(2)) //Getting the element on index 2 "blue"
//    println(colors)
//    println(colors[1])//Getting the element on index 1 "orange"
//    println(colors)
//    println(colors.contains("red"))//Checks if colors list contains the color "red"
//    println(colors)
//    println(colors.removeAt(2))//Removing an element "blue" using index
//    println(colors)
//    println(colors.remove("red"))//Removing an element using the name of the actual element
//    println(colors)
//    println(colors.add(2, "yellow"))//Adding an element by indicating the index it should be inserted and the element
//    println(colors)
//
//    //Imutable List
//    val sizes = listOf(6,7,8,9)
//    println(sizes.size)//Prints the size of the list
//    println(sizes.get(1))//Getting the element on index 1 "7"\
//    println(sizes.contains(9))//Checks if the list contains size "9"
//    println(sizes.isEmpty())//Checks if the size list is empty
//    println(sizes.reversed())//Reversed the size list from 6-9 to 9-6

    //Set -> Contains unique set of elements
    //Imutable
//    val usernames: Set<String> = setOf("brandon", "amelia", "charlie")
//    println(usernames)
//    println(usernames.size)
//    println(usernames)
//    println(usernames.contains("brandon"))

    //Mutable set
//    val students: MutableSet<String> = mutableSetOf("Brandon", "Amelia", "Charlie")
//    println(students)
//    students.add("Alex")
//    println(students)
//    println(students.add("Brandon"))//If this is a list, the result would be ("Brandon", "Amelia", "Charlie", "Brandon")
//    println(students)//But in list, it can't contain duplicate elements

    //Diff between list and set is that set checks if the element is duplicated on the .add function while list can contain duplicate values

    //Activity 2
    //Imutable Set
//    val name:Set<String> = setOf("Rommel", "Angel", "Melen")
//    println(name.size)//Return the size of the set
//    println(name.contains("Rommel"))//Checks if the set contains the element "Rommel"
//    println(name.reversed())//Reversed the set from Rommel-Melen to Melen-Rommel
//    println(name.isEmpty())//Check the set if empty
//    println(name.last())//get the last element

    //Mutable set
//    val surname: MutableSet<String> = mutableSetOf("Urbano", "Angeles", "Tamayo")
//    println(surname.size)//Checks the size of the set
//    println(surname.contains("Quintos"))//Checks if element "Quintos" is on the set
//    println(surname.isEmpty())//Checks if set is empty
//    println(surname.add("Quintos"))//Add element on the set
//    println(surname.reversed())//Reversing the order of set
//    println(surname.remove("Quintos"))//Remove element from the set
//    println(surname.last())//get the last element

    //Map -> Collection of key-value pairs
    //Student details: firstName, lastName, yearLevel, section
    //"Brandon", "Diaz", "first", "A"

    //Imutable Map
//    val studentDetails: Map<String, String> = mapOf(
//        "firstName" to "Brandon",
//        "lastName" to "Diaz",
//        "yearLevel" to "first",
//        "section" to "A"
//    )
//    println(studentDetails)
//    println(studentDetails.keys)
//    println(studentDetails.values)

//    //Mutable Map
//    val studentDetails: MutableMap<String, String> = mutableMapOf(
//        "firstName" to "Brandon",
//        "lastName" to "Diaz",
//        "yearLevel" to "first",
//        "section" to "A"
//    )
//    println(studentDetails)
//    println(studentDetails.remove("section"))

    //Imutable Map
    val addressDetails: Map<String, String> = mapOf(
        "number" to "two",
        "street" to "maceda",
        "barangay" to "sunflower"
    )
    println(addressDetails)
    println(addressDetails.values)//Prints the values of Map
    println(addressDetails.keys)//Prints the keys of Map
    println(addressDetails.get("number"))//Gets the value of a certain key
    println(addressDetails.getValue("street"))//Get the actual value of the key
    println(addressDetails.size)//Prints the size of the map
    println(addressDetails.contains("street"))//check if the map contains a certain key
    println(addressDetails.isEmpty())//Checks if map is empty
    println(addressDetails.containsKey("barangay"))//Checks if there is certain key
    println(addressDetails.containsValue("maceda"))//Checks if there is a certain value
    println(addressDetails.isNotEmpty())//Checks if the map is not empty

    //Mutable Map
    val phoneSpecs: MutableMap<String, String> = mutableMapOf(
        "brand" to "iphone",
        "color" to "pink",
        "version" to "mashmallow"
    )
    println(phoneSpecs)
    println(phoneSpecs["brand"])//get the value from the specific key
    println(phoneSpecs.size)//Get the map size
    println(phoneSpecs.isEmpty())//Checks if Map is empty
    phoneSpecs.replace("brand", "android")//Replacing the value from key brand
    println(phoneSpecs)
    println(phoneSpecs.containsValue("iphone"))//Checks if the value has certain value

}