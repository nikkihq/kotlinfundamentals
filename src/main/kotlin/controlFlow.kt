import java.util.*

fun main(){
//    try{
//        println("hi!")
//        println("Enter your name: ")
//        var name = readLine()!!
//        val guardian = true
//
//        //Ask for age
//        println("Hi $name Enter your age: ")
////    val input = Scanner(System.`in`)//FOR INTEGER
//        var age = readLine()!!.toInt()
//
//        if(age > 0){
//            if(age == 17){
//                println("You need a guardian")
//            }
//            else if(age < 18){
//                println("You're not allowed to use the service")
//            }
//            else{
//                println("Redirecting to services..")
//            }
//        }else{
//            println("Invalid Age value")
//        }
//    }catch (e: Exception){
//        println("Invalid Input")
//    }

    //Nested IF-ELSE IF-ELSE STATEMENT
//    println("What day is today?: ")
//    val dayOfWeek = readLine()!!
//
//
//    if (dayOfWeek == "Monday"){
//        println("Hey it's Monday! let's be productive")
//    }
//    else if(dayOfWeek == "tuesday"){
//        println("Hey it's Tuesday! let's be happy")
//    }
//    else if(dayOfWeek == "wednesday"){
//        println("Wednesday my Dudes!")
//    }
//    else if(dayOfWeek == "thursday"){
//        println("You're almost there!")
//    }
//    else if(dayOfWeek == "saturday" || dayOfWeek == "sunday"){
//        println("Happy weekends Choose to be happy")
//    }else{
//        println("Thank God it's Friday")
//    }


//    //WHEN expression
//    when(dayOfWeek){
//        "Monday" -> println("Hey it's Monday! let's be productive")
//        "Tuesday" -> println("Hey it's Tuesday! let's be happy")
//        "Wednesday" -> println("Wednesday my Dudes!")
//        "Thursday" -> println("You're almost there!")
//        "Friday" -> println("Thank God it's Friday")
//        "Saturday" -> println("Happy weekends Choose to be happy")
//        "Sunday" -> println("Happy weekends Choose to be happy")
//        else -> println("Not part of the week")
//
//    }
    //OR multiple lines in WHEN use {} curly braces


    //Calculator
    println("Welcome to calculator")
    print("Enter first value: ")
    val inputFirst = Scanner(System.`in`)
    val firstValue = inputFirst.nextDouble()
    print("Enter Second value: ")
    val inputSecond = Scanner(System.`in`)
    val secondValue = inputSecond.nextDouble()
    print("Enter operation: +,-,/,*: ")
    val operation = readLine()!!

    when(operation){
        "+" -> println(firstValue + secondValue)
        "-" -> println(firstValue - secondValue)
        "/" -> println(firstValue / secondValue)
        "*" -> println(firstValue * secondValue)
        else -> println("Operator $operation is not supported")
    }
}