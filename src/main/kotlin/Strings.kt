fun main(){
    //Simple string printing
    //Extract, combine, concatenate
    val sentence:String = "Happy Birthday! Justin!"

    //String functions
    println(sentence.get(0))//H
    println(sentence[1])//a
    println(sentence.subSequence(16, sentence.length))//Justin!
    println(sentence.substring(16, sentence.length))//Justin!
    println(sentence.length)//23
    println(sentence.plus(" God Bless!"))//Happy Birthday! Justin! God Bless!
    println(sentence.drop(6))//Birthday! Justin!
    println(sentence.dropLast(17))//Happy
    println(sentence.contains("Happy"))//True
    println(sentence.reversed())//True
    println(sentence.lowercase())
    println(sentence.uppercase())

    if(sentence == "Happy Birthday! Justin!"){
        println("Strings are the same")
    }
    else{
        println("String are not the same")
    }

    //String templates (String concatenation)
    val firstName = "Maria Nikki"
    val middleName = "Hacar"
    val lastName = "Quintos"

    println("My Name is: $firstName $middleName $lastName")
    println("My Name is: $firstName ${middleName.get(0)}. $lastName")

    val myAge = 22
    println("My age 10 years from now is: ${myAge+10}")


    //Practice
    val nickName = "nikki"
    val product = 100
    val amount = 10

    println("Hey ${nickName.uppercase().get(0) + nickName.substring(1)} " +
            "pay me ${product*amount} for the $product products you purchased")
    //Hey Nikki pay me 1000 for the 100 products you purchased


    val names = "Nikki is my name"
    val number = 123
    println(number.toString())
    println(names.split(" ").reversed().joinToString(" "))

    //Printing String
    val nameTwo:String = "Maria Nikki Quintos"
    println(names)
    println("Maria Nikki Quintos")

    println("--------------------------------------")

    //Printing Integer
    val b:Int = 10
    val c: String = "10"
    println("Printing Integer (int) type: " + b)
    println("Printing Integer (String) type: " + c)

    println("--------------------------------------")

    //Printing Character
    val d:Char = 'N'
    println("Printing Character: " + d)

    //Printing Escape characters
    val myString = "Nikki here!\n" //Inserting new line
    println("Insert Tab: Nikki\t") //Insert Tab
    println("Insert Backspace: Nikki\b") //Insert Backspace
    println("Insert double quote: \"Nikki\"") //Insert double quote
    println("Insert Backslash: Nikki\\") //Insert Backslash
    println("Inserting Dollar sign: \$300") //Inserting Dollar sign

    println("--------------------------------------")

    //Variable Declaration
    // Var can be re-use
    //Val cannot be re-use
    var myName = "Nikki"
    println("Printing My name using \"var\": " + myName)
    val mySurname = "Quintos"
    println("Printing My Surname using \"val\": " + mySurname)
    //Re-assigning a value to "var"
    myName = "Maria"
    println("Printing the Re-Assigned value to \"var\": " + myName)







}