fun main(){

    //Iteration using FOR loop
//    for(item in 1..5){
//        println(item)
//    }
//    val fruits = mapOf<Int,String>(1 to "Banana", 2 to "Apple", 3 to "Orange")
//    for(elements in fruits){
//        println(elements.value)
//    }

    //List inside a list
//    val users = arrayListOf<String>("Rom", "Mel")
//    val ages = arrayListOf<Int>(22, 23)

//    for(element in 0..users.size-1){
////        val username = users[element]
////        val age = ages[element]
////        println("$username is $age years old")
//        //or
//        println("${users[element]} is ${ages[element]} years old")
//    }


    //While loop
//    var exitCode:Int = -1
//
//    println("Enter number of people entered: ")
//    var userInput = readLine()!!.toInt()
//    var totalHumans = 0
//
//    while (userInput != exitCode){
//        totalHumans += userInput
//        println("*** $totalHumans ***")
//        userInput = readLine()!!.toInt()
//    }

    //DO-WHILE loop
//    var exitCode:Int = -1
//    println("Enter number of people entered: ")
//    var totalHumans = 0
//
//    do{
//        var userInput = readLine()!!.toInt()
//        if(userInput != exitCode){
//            totalHumans += userInput
//            println("*** $totalHumans ***")
//        }else{
//            println("Total Humans Entered: $totalHumans")
//            println("Thank you for using human counter")
//        }
//
//    }while (userInput != exitCode)

}
